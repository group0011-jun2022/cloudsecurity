# Identity Access Management (CYBERSECURITY)
## Tools
- Credential Report on an account level
- Access Advisor  on a user level

## Concepts
- 1 physical user = 1 aws user

## What is needed to have an IAM? (NOT TO USE ROOT USER)
 It is a combination of the following  policies and roles
 - Multi Factor Authentication (MFA)
 - Password policies
 - Role assignements
 - Using Access key for accessing the CLI/SDK
 - Credential reports for Auditors to access
 - Not to share the IAM user and access key information



 # Security Groups (Best practice have a seperate group only for SSH) (CYBERSECURITY)
 ## Concept
- Security groups regulate access to ports
- They authorize the IP ranges
- Control of inbound and inbound network traffic
- Acts like the network firewall

## DEFAULT RULES for in/out-bound rules
- Inbound rules [only authorized IPs are to come into the network via port 22] 
- Outbond rules [all IPs can travel out through any port]
## Important Porta To Memorize 
- 22 for SSH (secure shell) && SFTP (secure file transfer)
- 21 for FTP (file transfer)
- 80 for HTTP
- 443 for HTTPS (Secure HTTP)
- 3389 for RDP (remote desck top for windows)